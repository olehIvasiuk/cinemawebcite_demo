package com.ticket.cinemawebcite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaWebCiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaWebCiteApplication.class, args);
    }

}
