package com.ticket.cinemawebcite.controllers;

import com.ticket.cinemawebcite.models.MovieInfo;
import com.ticket.cinemawebcite.servises.FileStorageService;
import com.ticket.cinemawebcite.servises.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

@Controller
public class ControlPanelController {

    private final MovieService movieService;
    private final FileStorageService fileStorageService;

    @Autowired
    public ControlPanelController(MovieService movieService, FileStorageService fileStorageService) {
        this.movieService = movieService;
        this.fileStorageService = fileStorageService;
    }

    @GetMapping("/control-panel")
    public String dataManager(Model model) {
        return "/control/control-panel";
    }

    @GetMapping("/control-panel/create-movie")
    public String getCreateMovie(Model model) {
        return "control/create-movie";
    }

    @PostMapping("/control-panel/create-movie")
    public String movieCreatePost(@RequestParam String movieName, @RequestParam MultipartFile coverFile, @RequestParam String year, @RequestParam String genre,
                                  @RequestParam String allowableAge, @RequestParam String country, @RequestParam String duration,
                                  @RequestParam String ratingIMDB, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate rentalPeriodFrom,
                                  @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate rentalPeriodTo, @RequestParam String language,
                                  @RequestParam String originalName, @RequestParam String director, @RequestParam String budget,
                                  @RequestParam String starring, @RequestParam String movieDescription) {

        MovieInfo movieInfo = new MovieInfo(0, 0, movieName, year, genre, allowableAge, country,
                duration, ratingIMDB, rentalPeriodFrom, rentalPeriodTo, language, originalName, director, budget, starring, movieDescription);

        String coverPath = fileStorageService.saveFile(coverFile, "moviePosters");
        movieInfo.setCoverPath(coverPath);
        movieService.createMovie(movieInfo);
        return "redirect:/control-panel/movies";
    }

    @GetMapping("/control-panel/movies")
    public String getMovies(@RequestParam(name = "query", required = false) String query,
                            @RequestParam(name = "sort", required = false, defaultValue = "new_to_old") String sort, Model model) {

        if (query != null && !query.isEmpty()) {
            List<MovieInfo> movieSearchResult = movieService.searchMovies(query);
            model.addAttribute("movieSearchResult", movieSearchResult);
        }

        List<MovieInfo> movieInfos = switch (sort) {
            case "old_to_new" -> movieService.getAllMoviesSortedByOldToNew();
            case "a_to_z" -> movieService.getAllMoviesSortedByAToZ();
            case "z_to_a" -> movieService.getAllMoviesSortedByZToA();
            default -> movieService.getAllMoviesSortedByNewToOld();
        };
        model.addAttribute("movie", movieInfos);
        return "control/movies";
    }
}