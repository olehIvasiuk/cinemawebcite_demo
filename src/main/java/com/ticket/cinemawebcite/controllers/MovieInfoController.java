package com.ticket.cinemawebcite.controllers;

import com.ticket.cinemawebcite.models.MovieInfo;
import com.ticket.cinemawebcite.servises.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class MovieInfoController {

    private final MovieService movieService;

    @Autowired
    public MovieInfoController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/")
    public String movieInfo(Model model) {
        Iterable<MovieInfo> movieInfos = movieService.getAllMoviesSortedByNewToOld();
        model.addAttribute("movie", movieInfos);
        return "index";
    }

    @GetMapping("/movie-info/{id}")
    public String movieInfoDetails(@PathVariable(value = "id") long id, Model model) {
        Optional<MovieInfo> movieInfo = movieService.getMovieById(id);
        ArrayList<MovieInfo> res = new ArrayList<>();
        movieInfo.ifPresent(res::add);
        model.addAttribute("movieInfos", res);
        return "movie-info";
    }

    @PostMapping("/control-panel/movies/{id}")
    public String movieRemove(@PathVariable(value = "id") long id, Model model) {
        movieService.deleteMovie(id);
        return "redirect:/control-panel/movies";
    }
}