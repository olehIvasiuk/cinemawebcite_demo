package com.ticket.cinemawebcite.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.time.LocalDate;

@Entity
public class MovieInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int negVotesCount;
    private int posVotesCount;
    private String movieName;
    private String year;
    private String genre;
    private String allowableAge;
    private String country;
    private String duration;
    private String ratingIMDB;
    private LocalDate rentalPeriodFrom;
    private LocalDate rentalPeriodTo;
    private String language;
    private String originalName;
    private String director;
    private String budget;
    private String starring;
    private String movieDescription;
    private String coverPath;

    public MovieInfo() {}

    public MovieInfo(int negVotesCount, int posVotesCount, String movieName,String year,
                     String genre, String allowableAge, String country, String duration, String ratingIMDB,
                     LocalDate rentalPeriodFrom, LocalDate rentalPeriodTo, String language, String originalName, String director, String budget,
                     String starring, String movieDescription) {
        this.negVotesCount = negVotesCount;
        this.posVotesCount = posVotesCount;
        this.year = year;
        this.movieName = movieName;
        this.genre = genre;
        this.allowableAge = allowableAge;
        this.country = country;
        this.duration = duration;
        this.ratingIMDB = ratingIMDB;
        this.rentalPeriodFrom = rentalPeriodFrom;
        this.rentalPeriodTo = rentalPeriodTo;
        this.language = language;
        this.originalName = originalName;
        this.director = director;
        this.budget = budget;
        this.starring = starring;
        this.movieDescription = movieDescription;
        this.coverPath = coverPath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNegVotesCount() {
        return negVotesCount;
    }

    public void setNegVotesCount(int negVotesCount) {
        this.negVotesCount = negVotesCount;
    }

    public int getPosVotesCount() {
        return posVotesCount;
    }

    public void setPosVotesCount(int posVotesCount) {
        this.posVotesCount = posVotesCount;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAllowableAge() {
        return allowableAge;
    }

    public void setAllowableAge(String allowableAge) {
        this.allowableAge = allowableAge;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRatingIMDB() {
        return ratingIMDB;
    }

    public void setRatingIMDB(String ratingIMDB) {
        this.ratingIMDB = ratingIMDB;
    }

    public LocalDate getRentalPeriodFrom() {
        return rentalPeriodFrom;
    }

    public void setRentalPeriodFrom(LocalDate rentalPeriodFrom) {
        this.rentalPeriodFrom = rentalPeriodFrom;
    }

    public LocalDate getRentalPeriodTo() {
        return rentalPeriodTo;
    }

    public void setRentalPeriodTo(LocalDate rentalPeriodTo) {
        this.rentalPeriodTo = rentalPeriodTo;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getStarring() {
        return starring;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }
}
