package com.ticket.cinemawebcite.servises;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@Service
public class FileStorageService {

    @Value("${upload.path}")
    private String uploadPath;

    public String saveFile(MultipartFile file, String subFolder) {
        String fileName = "";
        if (file.isEmpty()) {
            return null;
        }
        try {
            byte[] bytes = file.getBytes();
            String folder = uploadPath + "/" + subFolder + "/";
            fileName = UUID.randomUUID() + "." + Objects.requireNonNull(file.getOriginalFilename()).replaceAll(" ", "");
            Path path = Paths.get(folder + fileName);
            Files.createDirectories(path.getParent());
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return subFolder + "/" + fileName;
    }
}