package com.ticket.cinemawebcite.servises;

import com.ticket.cinemawebcite.models.MovieInfo;
import com.ticket.cinemawebcite.repositories.MovieInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    private final MovieInfoRepository movieInfoRepository;

    @Autowired
    public MovieService(MovieInfoRepository movieInfoRepository) {
        this.movieInfoRepository = movieInfoRepository;
    }

    public List<MovieInfo> getAllMoviesSortedByOldToNew() {
        return movieInfoRepository.findAllByOrderByRentalPeriodFromAsc();
    }

    public List<MovieInfo> getAllMoviesSortedByNewToOld() {
        return movieInfoRepository.findAllByOrderByRentalPeriodFromDesc();
    }

    public List<MovieInfo> getAllMoviesSortedByAToZ() {
        return movieInfoRepository.findAllByOrderByMovieNameAsc();
    }

    public List<MovieInfo> getAllMoviesSortedByZToA() {
        return movieInfoRepository.findAllByOrderByMovieNameDesc();
    }

    public List<MovieInfo> searchMovies(String searchString) {
        return movieInfoRepository.findAllByMovieNameContainingIgnoreCase(searchString);
    }

    public void createMovie(MovieInfo movieInfo) {
        movieInfoRepository.save(movieInfo);
    }

    public Optional<MovieInfo> getMovieById(Long id) {
        return movieInfoRepository.findById(id);
    }

    public void deleteMovie(Long id) {
        MovieInfo movieInfo = movieInfoRepository.findById(id).orElseThrow();
        movieInfoRepository.delete(movieInfo);
    }
}