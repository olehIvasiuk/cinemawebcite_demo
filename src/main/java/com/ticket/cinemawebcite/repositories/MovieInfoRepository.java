package com.ticket.cinemawebcite.repositories;

import com.ticket.cinemawebcite.models.MovieInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MovieInfoRepository extends CrudRepository<MovieInfo, Long> {
    List<MovieInfo> findAllByOrderByRentalPeriodFromAsc();
    List<MovieInfo> findAllByOrderByRentalPeriodFromDesc();
    List<MovieInfo> findAllByOrderByMovieNameAsc();
    List<MovieInfo> findAllByOrderByMovieNameDesc();
    List<MovieInfo> findAllByMovieNameContainingIgnoreCase(String searchString);
}
