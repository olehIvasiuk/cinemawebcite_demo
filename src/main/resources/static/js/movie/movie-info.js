const likeButton = document.getElementById('like-button')
const dislikeButton = document.getElementById('dislike-button')
const likeCount = document.getElementById('like-count')
const dislikeCount = document.getElementById('dislike-count')
const positiveBar = document.getElementById('positive-bar')
const negativeBar = document.getElementById('negative-bar')

let positiveVotes = 0
let negativeVotes = 0

likeButton.addEventListener('click', () => {
    positiveVotes++
    updateBars()
})

dislikeButton.addEventListener('click', () => {
    negativeVotes++
    updateBars()
})

function updateBars() {
    const totalVotes = positiveVotes + negativeVotes
    const positivePercentage = (positiveVotes / totalVotes) * 100
    const negativePercentage = (negativeVotes / totalVotes) * 100

    positiveBar.style.width = `${positivePercentage}%`
    negativeBar.style.width = `${negativePercentage}%`

    likeCount.textContent = positiveVotes
    dislikeCount.textContent = negativeVotes

    if(positiveVotes > 9) {
        likeButton.style.width = '4.8rem'
    }
    if (positiveVotes > 99) {
        likeButton.style.width = '5.5rem'
    }

    if (negativeVotes > 9) {
        dislikeButton.style.width = '4.8rem'
    }
    if (negativeVotes > 99) {
        dislikeButton.style.width = '5.5rem'
    }
}


const firstInfoColumn = document.getElementById('mid-info-column1')
const secondInfoColumn = document.getElementById('mid-info-column2')
const movieCover = document.getElementById('mid-movie-cover')

const movieRating = document.getElementById('mid-movie-rating')

const movieActionsList = document.getElementById('mid-actions__list')
const popupIsland = document.getElementById('mid-sticky-actions')


function handleMedia1023(e) {
    if (e.matches) {
        window.addEventListener('scroll', floatingSecondColumn);
    }else {
        secondInfoColumn.style.top = `${75}px`
        window.removeEventListener('scroll', floatingSecondColumn);
    }
}
function floatingSecondColumn() {
    if (header.classList.contains('hide')){
        secondInfoColumn.style.top = `${10}px`
    }else {
        secondInfoColumn.style.top = (header.offsetHeight + 10) + 'px'
    }
}

const mediaQueryMovieInfo1023 = window.matchMedia('(max-width: 1023px)');
mediaQueryMovieInfo1023.addListener(handleMedia1023);
handleMedia1023(mediaQueryMovieInfo1023);


function moveMovieCover(e) {
    if (e.matches) {
        firstInfoColumn.insertBefore(movieCover, firstInfoColumn.firstChild)
        firstInfoColumn.insertBefore(movieRating, firstInfoColumn.lastElementChild)
        popupIsland.insertBefore(movieActionsList, popupIsland.firstChild)
    } else {
        secondInfoColumn.insertBefore(movieCover, secondInfoColumn.firstChild)
        secondInfoColumn.insertBefore(movieRating, secondInfoColumn.childNodes[1])
        secondInfoColumn.insertBefore(movieActionsList, secondInfoColumn.lastChild)
    }
}

const mediaQueryMovieInfo900 = window.matchMedia('(max-width: 900px)')
mediaQueryMovieInfo900.addListener(moveMovieCover)
moveMovieCover(mediaQueryMovieInfo900)