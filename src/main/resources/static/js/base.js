
//move elements
const header = document.querySelector('.cp_header-wrapper')
const headerNavigation = document.getElementById('hid-header-nav');
const headerNavList = document.getElementById('hid-header__list')
const rightGroup = document.getElementById('hid-right-group')
const searchContainer = document.getElementById('hid-form-search')
const headerLocation = document.getElementById('hid-location__list')
const headerUserManager = document.getElementById('hid-user-manager')
const menuMobile = document.getElementById('hid-menu-mobile')
const menuMobileContainer = document.getElementById('hid-m-container');
const menuMobileContent = document.getElementById('hid-m-content')

const moveElement = (element, destination) => destination.insertBefore(element, destination.firstChild)
let lastScrollPosition = 0;


//media 1023
function handleMedia1023(e) {
    if (e.matches) {
        moveInMobileMenu()
        window.addEventListener('scroll', floatingHeader)
    } else {
        moveFromMobileMenu()
        window.removeEventListener('scroll', floatingHeader)
    }
}

function moveInMobileMenu() {
    if (searchContainer.parentNode === rightGroup) {
        moveElement(searchContainer, menuMobile)
        moveElement(headerLocation, menuMobileContainer)
        moveElement(headerUserManager, menuMobileContent)
    }
}
function moveFromMobileMenu() {
    if (searchContainer.parentNode === menuMobile) {
        moveElement(headerUserManager, rightGroup)
        moveElement(headerLocation, rightGroup)
        moveElement(searchContainer, rightGroup)
    }
}
function floatingHeader() {
    const scrollPosition = () => window.pageYOffset || document.documentElement.scrollTop
    const containHide = () => header.classList.contains('hide')
    const scrollThreshold = 50
    const currentScrollPosition = scrollPosition()
    if (Math.abs(currentScrollPosition - lastScrollPosition) >= scrollThreshold) {
        if (currentScrollPosition > lastScrollPosition && !containHide()) {
            header.classList.add('hide')
        } else if (currentScrollPosition < lastScrollPosition && containHide()) {
            header.classList.remove('hide')
        }
        lastScrollPosition = currentScrollPosition
    }
}

const mediaQuery1023 = window.matchMedia('(max-width: 1023px)');
mediaQuery1023.addListener(handleMedia1023);
handleMedia1023(mediaQuery1023);


//media 767
function handleMedia767(e) {
    if (e.matches) {
        moveHeaderNavInMobileMenu();
    } else {
        moveNavListFromMobileMenu();
    }
}

function moveHeaderNavInMobileMenu() {
    if (headerNavList.parentNode === headerNavigation) {
        moveElement(headerNavList, menuMobileContent)
    }
}
function moveNavListFromMobileMenu() {
    if (headerNavList.parentNode === menuMobileContent) {
        moveElement(headerNavList, headerNavigation)
    }
}

const mediaQuery767 = window.matchMedia('(max-width: 767px)');
mediaQuery767.addListener(handleMedia767);
handleMedia767(mediaQuery767);


//aside menu activate
document.addEventListener('DOMContentLoaded', function () {
    const body = document.querySelector('body')
    const openPopupBtn = document.getElementById('hid-menu__toggle');
    const closePopupBtn = document.getElementById('hid-menu__toggle-close');
    let visibleAnimation = 'slideIn 0.5s forwards';
    let hiddenAnimation = 'slideOut 0.5s forwards';

    function toggleMenu(display, right, animation, addListener) {
        menuMobileContainer.style.visibility = display;
        menuMobileContainer.style.right = right;
        menuMobileContainer.style.animation = animation;

        if (addListener) {
            document.addEventListener('click', closePopupOnClickOutside);
        } else {
            document.removeEventListener('click', closePopupOnClickOutside);
        }
    }

    function closePopupOnClickOutside(event) {
        if (!menuMobileContainer.contains(event.target) && event.target !== openPopupBtn) {
            toggleMenu('hidden', '', hiddenAnimation, false);
            openPopupBtn.checked = false;
            closePopupBtn.checked = false;
            disableScrollBody(false)
        }
    }

    openPopupBtn.addEventListener('change', function () {
        if (openPopupBtn.checked) {
            disableScrollBody(true)
            closePopupBtn.checked = true;
            toggleMenu('visible', '0', visibleAnimation, true);
        } else {
            toggleMenu('hidden', '', hiddenAnimation, false);
        }
    });

    closePopupBtn.addEventListener('change', function () {
        if (closePopupBtn.checked) {
            toggleMenu('visible', '0', visibleAnimation, true);
        } else {
            disableScrollBody(false)
            toggleMenu('hidden', '', hiddenAnimation, false);
            openPopupBtn.checked = false;
        }
    });

    function disableScrollBody(show) {
        show ? body.classList.add('scroll-hidden') : body.classList.remove('scroll-hidden');
    }
});


//animate open search
const headerLogo = document.getElementById('hid-header-logo');
const submitSearchBtn = document.getElementById('hid-form__button');
const searchInput = document.getElementById('hid-form__input');

function toggleSearch(show) {
    const displayStyleSearch = show ? 'flex' : 'none';
    const displayStyleL = show ? 'none' : 'flex';
    const borderRadiusInput = show ? '8px 0 0 8px' : '8px';
    const borderRadiusBtn = show ? '0 8px 8px 0' : '8px';

    searchInput.style.display = displayStyleSearch;
    searchInput.style.borderRadius = borderRadiusInput;
    headerLogo.style.display = displayStyleL;
    submitSearchBtn.style.borderRadius = borderRadiusBtn;
    if (show) {
        searchInput.focus();
    }
}

function inputSearchActivate() {
    const isSearchHidden = searchInput.style.display === 'none';
    toggleSearch(isSearchHidden);
    showOrHideNavList();
}

function hideInputSearchOnClickOutside(event) {
    if (!event.target.closest('#hid-form-search') && window.matchMedia('(max-width: 900px)').matches) {
        toggleSearch(false);
        showOrHideNavList();
    }
}

function showOrHideNavList() {
    if (headerLogo.style.display === 'none'){
        headerNavigation.style.display = 'none';
    }else {
        headerNavigation.style.display = 'flex';
    }
}

function resetStylesOnResize() {
    searchInput.style.display = 'flex';
    searchInput.style.borderRadius = '8px 0 0 8px';
    headerNavigation.style.display = 'flex';
    headerLogo.style.display = 'flex';
    submitSearchBtn.style.borderRadius = '0 8px 8px 0';
}

submitSearchBtn.addEventListener('click', inputSearchActivate);
document.addEventListener('click', hideInputSearchOnClickOutside);

function handleMedia900px(e) {
    if (e.matches) {
        toggleSearch(false);
        submitSearchBtn.addEventListener('click', inputSearchActivate);
    } else {
        resetStylesOnResize();
        submitSearchBtn.removeEventListener('click', inputSearchActivate);
    }
}

const mediaQuery900px = window.matchMedia('(max-width: 900px)');
mediaQuery900px.addListener(handleMedia900px);
handleMedia900px(mediaQuery900px);