class ButtonHandler {
    constructor(selector) {
        this.buttons = document.querySelectorAll(selector);
        this.activate = this.activate.bind(this);
        this.deactivate = this.deactivate.bind(this);
        this.init();
    }

    init() {
        this.buttons.forEach(button => {
            button.addEventListener('touchstart', this.activate);
            button.addEventListener('touchend', this.deactivate);
            button.addEventListener('mousedown', this.activate);
            button.addEventListener('mouseup', this.deactivate);
            button.addEventListener('mouseleave', this.deactivate);
        });
    }

    activate(event) {
        event.currentTarget.classList.add('active');
    }

    deactivate(event) {
        event.currentTarget.classList.remove('active');
    }
}

document.addEventListener('DOMContentLoaded', function() {
    new ButtonHandler('.btn');
});










// class FormValidator {
//     constructor(formSelector, contSelector, fields, patterns) {
//         this.form = document.querySelector(formSelector);
//         if (!this.form) return;
//
//         this.fields = fields;
//         this.patterns = patterns;
//         this.rentalPeriodFrom = this.form.querySelector('#rental-period-from__inp');
//         this.rentalPeriodTo = this.form.querySelector('#rental-period-to__inp');
//         this.setupListeners(contSelector);
//     }
//
//     setupListeners(contSelector) {
//         const fieldsArray = Object.keys(this.fields);
//
//         fieldsArray.forEach(fieldName => {
//             const field = this.form.querySelector(this.fields[fieldName]);
//             const cont = field.closest(contSelector);
//             if (field) {
//                 field.addEventListener('input', () => this.validateField(fieldName, field, cont));
//             }
//             if (this.rentalPeriodFrom && this.rentalPeriodTo) {
//                 this.rentalPeriodFrom.addEventListener('input', () => {
//                     this.rentalPeriodTo.setAttribute('min', this.rentalPeriodFrom.value);
//                 });
//
//                 this.rentalPeriodTo.addEventListener('input', () => {
//                     this.rentalPeriodFrom.setAttribute('max', this.rentalPeriodTo.value);
//                 });
//             }
//
//         });
//     }
//
//     validateField(fieldName, field, cont) {
//         const pattern = this.patterns[fieldName];
//         if (!pattern.test(field.value)) {
//             cont.classList.add('invalid');
//             field.setCustomValidity('');
//             field.reportValidity();
//             console.log('invalid')
//         } else {
//             cont.classList.remove('invalid');
//             field.setCustomValidity('');
//             console.log('valid')
//         }
//     }
// }
//
// const fields = {
//     movieName: '#movie-name__inp',
//     year: '#year__inp',
//     duration: '#duration__inp',
//     ratingIMDB: '#ratingIMDB__inp',
//     originalName: '#original-name__inp',
//     director: '#director__inp',
//     budget: '#budget__inp',
//     starring: '#starring__inp',
// }
// const patterns = {
//     movieName: /^(?!.*[\\?!.,:-]{2})[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ\s\\?!.,:-]+$/,
//     year: /^\d{4}$/,
//     duration: /^(?:[0-4]?\d{1,2}|500)$/,
//     ratingIMDB: /^((10|[0-9](\.\d{1,2})?)\/(10|[0-9](\.\d{1,2})?))$/,
//     originalName: /^(?!.*[\\?!.,:-]{2})[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ\s\\?!.,:-]+$/,
//     director: /^[а-яА-Яa-zA-ZґҐєЄіІїЇ\s]+$/u,
//     budget: /^[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ$€£¥₴\s]+$/u,
//     starring: /^[а-яА-Яa-zA-ZґҐєЄіІїЇ,.«»"\s]+$/u,
// }
//
//
// new FormValidator('.cp_form--create-movie', '.field-component', fields, patterns);
















class FormValidator {
    constructor(formSelector, contSelector, fields, patterns) {
        this.form = document.querySelector(formSelector);
        if (!this.form) return;

        this.fields = fields;
        this.patterns = patterns;
        this.rentalPeriodFrom = this.form.querySelector('#rental-period-from__inp');
        this.rentalPeriodTo = this.form.querySelector('#rental-period-to__inp');
        this.setupListeners(contSelector);
        this.form.addEventListener('submit', (event) => this.handleSubmit(event));
    }

    setupListeners(contSelector) {
        const fieldsArray = Object.keys(this.fields);

        fieldsArray.forEach(fieldName => {
            const field = this.form.querySelector(this.fields[fieldName]);
            const cont = field.closest(contSelector);
            if (field) {
                field.addEventListener('input', () => this.validateField(fieldName, field, cont));
            }
        });

        if (this.rentalPeriodFrom && this.rentalPeriodTo) {
            this.rentalPeriodFrom.addEventListener('input', () => {
                this.rentalPeriodTo.setAttribute('min', this.rentalPeriodFrom.value);
                this.validateRentalPeriod();
            });

            this.rentalPeriodTo.addEventListener('input', () => {
                this.rentalPeriodFrom.setAttribute('max', this.rentalPeriodTo.value);
                this.validateRentalPeriod();
            });
        }
    }

    validateField(fieldName, field, cont) {
        const pattern = this.patterns[fieldName];
        if (!pattern.test(field.value)) {
            cont.classList.add('invalid');
            field.setCustomValidity('Некорректное значение');
            field.reportValidity();
            console.log('invalid')
        } else {
            cont.classList.remove('invalid');
            field.setCustomValidity('');
            console.log('valid')
        }
    }

    validateRentalPeriod() {
        const fromValid = this.rentalPeriodFrom.value !== '';
        const toValid = this.rentalPeriodTo.value !== '';
        const fromCont = this.rentalPeriodFrom.closest('.field-component');
        const toCont = this.rentalPeriodTo.closest('.field-component');

        if (fromValid && toValid) {
            fromCont.classList.remove('invalid');
            this.rentalPeriodFrom.setCustomValidity('');
            toCont.classList.remove('invalid');
            this.rentalPeriodTo.setCustomValidity('');
        } else {
            if (!fromValid) {
                fromCont.classList.add('invalid');
                this.rentalPeriodFrom.setCustomValidity('Заполните это поле');
            }
            if (!toValid) {
                toCont.classList.add('invalid');
                this.rentalPeriodTo.setCustomValidity('Заполните это поле');
            }
        }
    }

    handleSubmit(event) {
        const fieldsArray = Object.keys(this.fields);
        let formIsValid = true;

        fieldsArray.forEach(fieldName => {
            const field = this.form.querySelector(this.fields[fieldName]);
            const cont = field.closest('.field-component');
            if (!this.patterns[fieldName].test(field.value)) {
                this.validateField(fieldName, field, cont);
                formIsValid = false;
            }
        });

        this.validateRentalPeriod();

        if (!formIsValid) {
            event.preventDefault();
            alert('Пожалуйста, исправьте ошибки в форме перед отправкой.');
        }
    }
}

const fields = {
    movieName: '#movie-name__inp',
    year: '#year__inp',
    duration: '#duration__inp',
    ratingIMDB: '#ratingIMDB__inp',
    originalName: '#original-name__inp',
    director: '#director__inp',
    budget: '#budget__inp',
    starring: '#starring__inp',
};

const patterns = {
    movieName: /^(?!.*[\\?!.,:-]{2})[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ\s\\?!.,:-]+$/,
    year: /^\d{4}$/,
    duration: /^(?:[0-4]?\d{1,2}|500)$/,
    ratingIMDB: /^((10|[0-9](\.\d{1,2})?)\/(10|[0-9](\.\d{1,2})?))$/,
    originalName: /^(?!.*[\\?!.,:-]{2})[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ\s\\?!.,:-]+$/,
    director: /^[а-яА-Яa-zA-ZґҐєЄіІїЇ\s]+$/u,
    budget: /^[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ$€£¥₴\s]+$/u,
    starring: /^[а-яА-Яa-zA-ZґҐєЄіІїЇ,.«»"\s]+$/u,
};

new FormValidator('.cp_form--create-movie', '.field-component', fields, patterns);



















// -----------------------------------------------------------------------

class ImageUploader {
    constructor(fileInputId, fileLabelClass, imagePreviewId) {
        this.fileInput = document.getElementById(fileInputId);
        this.fileLabelInput = document.querySelector(fileLabelClass);
        this.imagePreview = document.getElementById(imagePreviewId);
        if (!this.fileInput || !this.fileLabelInput || !this.imagePreview) return;
        this.fileLabelInput.addEventListener('click', this.handleFileLabelClick.bind(this));
    }

    handleFileLabelClick() {
        if (this.fileInput.value.length > 0) {
            this.reset()
        } else {
            this.fileInput.addEventListener('change', this.handleFileInputChange.bind(this));
        }
    }
    handleFileInputChange() {
        const file = this.fileInput.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = () => {
                this.imagePreview.src = reader.result;
            };
            reader.readAsDataURL(file);
            this.fileLabelInput.style.backgroundColor = 'var(--red)';
            this.fileLabelInput.textContent = 'Видалити постер';
        }
    }
    reset() {
        this.fileInput.value = '';
        this.imagePreview.src = '/img/add-image.svg';
        this.fileLabelInput.textContent = 'Додати постер';
        event.preventDefault();
        this.fileLabelInput.style.backgroundColor = 'var(--green)';
    }
}
new ImageUploader('file-upload', '.custom-file-upload', 'poster');

// -----------------------------------------------------------------------

document.addEventListener('DOMContentLoaded', () => {
    class MenuToggle {
        constructor(toggleBtnId, menuContainerId, mainWrapperId) {
            this.body = document.body;
            this.toggleBtn = document.getElementById(toggleBtnId);
            this.menuContainer = document.getElementById(menuContainerId);
            this.mainWrapper = document.getElementById(mainWrapperId);

            if (!this.toggleBtn || !this.menuContainer || !this.mainWrapper) {
                console.error('One or more elements not found!');
                return;
            }

            this.isOpen = false;
            this.previousOverflow = '';

            this.toggleBtn.addEventListener('click', () => this.toggleMenu());
            document.addEventListener('click', (event) => this.closePopupOnClickOutside(event));
        }

        toggleMenu() {
            this.isOpen = !this.isOpen;
            this.updateMenuState();
        }

        updateMenuState() {
            if (this.isOpen) {
                this.previousOverflow = this.body.style.overflow;
                this.disableScrollBody(true);
                this.menuContainer.style.animationName = 'slideIn';
            } else {
                this.menuContainer.style.animationName = 'slideOut';
                this.body.style.overflow = this.previousOverflow;
            }

            this.menuContainer.classList.toggle('opened', this.isOpen);
            this.toggleBtn.classList.toggle('opened', this.isOpen);

            this.mainWrapper.style.filter = this.isOpen ? 'blur(5px) brightness(0.8)' : '';
            this.mainWrapper.style.zIndex = this.isOpen ? '-50' : '1';
            this.disableFocus(this.isOpen);
        }

        closePopupOnClickOutside(event) {
            if (this.isOpen && !this.menuContainer.contains(event.target) && !this.toggleBtn.contains(event.target)) {
                this.toggleMenu();
            }
        }

        disableScrollBody(show) {
            this.body.classList.toggle('overflow-hidden', show);
        }

        disableFocus(flag) {
            this.mainWrapper.querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])')
                .forEach(el => el.setAttribute('tabindex', flag ? '-1' : '0'));
        }
    }

    new MenuToggle('menu__btn', 'menu-side', 'main-w');
});


class AccordionMenu {
    constructor(target, config) {
        this.el = typeof target === 'string' ? document.querySelector(target) : target
        const defaultConfig = { alwaysOpen: true, duration: 350}
        this.config = Object.assign(defaultConfig, config)
        this.addEventListener()
    }

    addEventListener() {
        this.el.addEventListener('click', e => {
            const elHeader = e.target.closest('.cp_accordion--header')
            if (!elHeader) {
                return
            }
            if (!this.config.alwaysOpen) {
                const elOpenItem = this.el.querySelector('.cp_accordion--item--show')
                if (elOpenItem) {
                    elOpenItem !== elHeader.parentElement ? this.toggle(elOpenItem) : null
                }
            }
            this.toggle(elHeader.parentElement)
        })
    }

    show(el) {
        const elBody = el.querySelector('.cp_accordion--body')
        const elHeader = el.querySelector('.cp_accordion--header')
        if (elBody.classList.contains('collapsing') || el.classList.contains('cp_accordion--item--show')) {
            return
        }
        elHeader.style.borderRadius = '.75rem .75rem 0 0';
        elBody.style.display = 'block'
        const height = elBody.offsetHeight
        elBody.style.height = '0'
        elBody.style.overflow = 'hidden'
        elBody.style.transition= `height ${this.config.duration}ms ease`
        elBody.classList.add('collapsing')
        el.classList.add('cp_accordion--item--expand')
        elBody.offsetHeight
        elBody.style.height = `${height}px`
        window.setTimeout(() => {
            elBody.classList.remove('collapsing')
            el.classList.remove('cp_accordion--item--expand')
            elBody.classList.add('collapse')
            el.classList.add('cp_accordion--item--show')
            elBody.style.display = ''
            elBody.style.height = ''
            elBody.style.transition = ''
            elBody.style.overflow = ''
        }, this.config.duration)
    }

    hide(el) {
        const elBody = el.querySelector('.cp_accordion--body')
        const elHeader = el.querySelector('.cp_accordion--header')
        if (elBody.classList.contains('collapsing') || !el.classList.contains('cp_accordion--item--show')) {
            return
        }
        elBody.style.height = `${elBody.offsetHeight}px`
        elBody.offsetHeight
        elBody.style.display = 'block'
        elBody.style.height = '0'
        elBody.style.overflow = 'hidden'
        elBody.style.transition = `height ${this.config.duration}ms ease`
        elBody.classList.remove('collapse')
        el.classList.remove('cp_accordion--item--show')
        elBody.classList.add('collapsing')
        window.setTimeout(() => {
            elBody.classList.remove('collapsing')
            elBody.classList.add('collapse')
            elBody.style.display = ''
            elBody.style.height = ''
            elBody.style.transition = ''
            elBody.style.overflow = ''
            elHeader.style.borderRadius = ''
        }, this.config.duration)
    }

    toggle(el) {
        el.classList.contains('cp_accordion--item--show') ? this.hide(el) : this.show(el)
    }
}

new AccordionMenu(document.querySelector('.cp_accordion'), {
    alwaysOpen: false,
})

// -----------------------------------------------------------------------

document.addEventListener('DOMContentLoaded', function () {
    const movieTools = document.querySelectorAll('.mm-popup-cont.movie-tools');
    movieTools.forEach(tool => new MoreMenu(tool));

    const searchComponents = document.querySelectorAll('.movies-search__comp');
    searchComponents.forEach(container => new SearchPopup(container));

    const sortComponents = document.querySelectorAll('.movie-sort-tool');
    sortComponents.forEach(container => new MovieSorter(container));
});

class MoreMenu {
    constructor(tool) {
        if (!tool) return;

        const moreBtn = tool.querySelector('.movie-more__btn');
        const morePopup = tool.querySelector('.more-menu__popup');
        new Popup(moreBtn, morePopup);

        const kmBtnDelete = tool.querySelector('.btn.popup__btn.delete');
        this.cardId = tool.dataset.cardId;
        kmBtnDelete.addEventListener('click', () => this.deleteCard());
    }

    deleteCard() {
        if (confirm("Дійсно видалити?")) {
            fetch('/control-panel/movies/' + this.cardId, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    cardId: this.cardId
                })
            })
                .then(response => {
                    if (response.ok) {
                        window.location.reload();
                    } else {
                        throw new Error('Произошла ошибка при удалении.');
                    }
                })
                .catch(error => {
                    console.error('Ошибка:', error);
                });
        }
    }
}

class SearchPopup {
    constructor(container) {
        this.container = container;
        this.searchInput = this.container.querySelector('.search__inp');
        this.searchPreviewPopup = this.container.querySelector('.search-preview-popup');
        this.customButton = this.container.querySelector('.search__btn');
        this.customTips = this.container.querySelector('.search-tip');

        this.init();
    }

    init() {
        new Popup(this.searchInput, this.searchPreviewPopup);
        this.searchInput.addEventListener('input', () => this.searchMovies());
    }

    async searchMovies() {
        const searchString = this.searchInput.value;
        try {
            const response = await fetch(`/control-panel/movies?query=${encodeURIComponent(searchString)}`);
            if (!response.ok) throw new Error('Network response was not ok');
            const html = await response.text();
            const parser = new DOMParser();
            const doc = parser.parseFromString(html, "text/html");
            const movieCards = doc.querySelectorAll('.search-result__item');

            this.searchPreviewPopup.innerHTML = '';
            this.searchPreviewPopup.appendChild(this.customTips);
            if (movieCards.length > 0) {
                movieCards.forEach(card => {
                    this.searchPreviewPopup.appendChild(card);
                });
                this.customTips.classList.add('hide');
            } else {
                this.customTips.classList.remove('hide');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    }
}


class Popup {
    constructor(trigger, popup) {
        this.trigger = trigger;
        this.popup = popup;
        this.isOpen = false;

        if (!this.trigger || !this.popup) {
            console.error('Popup trigger or popup not found!');
            return;
        }

        this.init();
    }

    init() {
        this.trigger.addEventListener('click', () => this.showPopup());
        window.addEventListener('mousedown', (e) => this.windowOnClick(e));
        window.addEventListener('keydown', (e) => this.windowOnKeyDown(e));
        this.popup.addEventListener('click', (e) => this.stopPropagation(e));
    }

    showPopup() {
        this.isOpen = true;
        this.popup.classList.add('visible');
    }

    hidePopup() {
        if (this.isOpen) {
            this.isOpen = false;
            this.popup.classList.remove('visible');
        }
    }

    windowOnClick(event) {
        if (this.isOpen && event.target !== this.popup
            && !this.popup.contains(event.target)
            && event.target !== this.trigger) {
            this.hidePopup();
        }
    }

    windowOnKeyDown(event) {
        if (this.isOpen && event.key === 'Escape') {
            this.hidePopup();
        }
    }

    stopPropagation(event) {
        event.stopPropagation();
    }
}



class MovieSorter {
    constructor(container) {
        if (!container) return;

        const sortBtn = container.querySelector('.sorting-more__btn');
        const sortPopup = container.querySelector('.mm-popup');
        new Popup(sortBtn, sortPopup);

        this.container = container;
        this.initSortParam();
        this.setupSortRadio();
        this.sortMovies = this.sortMovies.bind(this);
    }

    initSortParam() {
        const urlParams = new URLSearchParams(window.location.search);
        this.sortParam = urlParams.get('sort');
    }

    setupSortRadio() {
        if (this.sortParam) {
            const selectedRadioButton = this.container.querySelector(`input[name="sort"][value="${this.sortParam}"]`);
            if (selectedRadioButton) {
                selectedRadioButton.checked = true;
            }
        }

        this.container.querySelectorAll('input[name="sort"]').forEach(radioBtn => {
            radioBtn.addEventListener('change', () => {
                this.sortMovies(radioBtn.value);
            });
        });
    }

    sortMovies(sortBy) {
        window.location.href = "/control-panel/movies?sort=" + sortBy;
    }
}



// -----------------------------------------------------------------------


class CustomSelect {
    constructor(originalSelect) {
        this.originalSelect = originalSelect
        this.customInput = document.createElement('input')
        this.customInput.classList.add('dropdown__input', 'dropdown-select', 'inp-area')
        this.customInput.type = 'text'
        this.customInput.id = `${this.originalSelect.name}__inp`
        this.customInput.placeholder = ''
        this.customInput.readOnly = true
        this.customInput.required = true
        this.customInput.maxLength = this.originalSelect.getAttribute('max-length')
        this.customInput.autocomplete = 'off'
        this.customLabel = document.createElement('label')
        this.customLabel.classList.add('text__label')
        this.customLabel.textContent = this.originalSelect.getAttribute('data-label')
        this.customLabel.htmlFor = `${this.originalSelect.name}__inp`
        this.customSelect = document.createElement('div')
        this.customSelect.classList.add('select')

        this.customInput.addEventListener('click', () => {
            if (this.customSelect.style.display === 'none') {
                this.customSelect.style.display = 'flex'
                this.customSelect.style.height = '200px'
            } else {
                this.customSelect.style.display = 'none'
                this.customSelect.style.height = '0'
            }
        })

        this.customInput.addEventListener('input', () => {
            this.originalSelect.value = this.customInput.value
        })

        this.originalSelect.querySelectorAll('option').forEach(optionElement => {
            const itemElement = document.createElement('div')
            itemElement.classList.add('select__item')
            itemElement.textContent = optionElement.textContent
            this.customSelect.appendChild(itemElement)

            if (optionElement.selected) {
                this._select(itemElement)
            }

            itemElement.addEventListener('click', () => {
                if (
                    this.originalSelect.multiple &&
                    itemElement.classList.contains('select__item-selected')
                ) {
                    this._deselect(itemElement)
                } else {
                    this._select(itemElement)
                }
            })
        })

        this.originalSelect.insertAdjacentElement('afterend', this.customSelect)
        this.originalSelect.insertAdjacentElement('afterend', this.customLabel)
        this.originalSelect.insertAdjacentElement('afterend', this.customInput)
        this.customSelect.style.display = 'none'
    }

    _select(itemElement) {
        const index = Array.from(this.customSelect.children).indexOf(itemElement)

        if (!this.originalSelect.multiple) {
            this.customSelect.querySelectorAll('.select__item').forEach(el => {
                el.classList.remove('select__item-selected')
            })
        }

        this.originalSelect.querySelectorAll('option')[index].selected = true
        itemElement.classList.add('select__item-selected')

        this._updateInputValue()
    }

    _deselect(itemElement) {
        const index = Array.from(this.customSelect.children).indexOf(itemElement)

        this.originalSelect.querySelectorAll('option')[index].selected = false
        itemElement.classList.remove('select__item-selected')

        this._updateInputValue()
    }

    _updateInputValue() {
        const selectedOptions = Array.from(
            this.originalSelect.querySelectorAll('option:checked')
        ).map(option => option.value)
        this.customInput.value = selectedOptions.join(', ')
    }
}

document.querySelectorAll('.dropdown').forEach(selectElement => {
    new CustomSelect(selectElement)
})