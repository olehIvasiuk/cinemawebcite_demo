document.addEventListener('DOMContentLoaded', function () {
    const tools = document.querySelectorAll('.movie-tool-controls')
    tools.forEach(tool => {
        const controlType = tool.dataset.control || 'more'
        new MoreMenu(tool, controlType)
    })
})

class MoreMenu {
    constructor(tool, controlType) {
        if (!tool) {return}

        // this.cardId = tool.dataset.cardId;
        // this.customDiv = document.createElement('div')
        // this.customDiv.classList.add('tool-controls')

        // this.customButton = document.createElement('button')
        // this.customButton.classList.add('btn', 'more__btn')
        // this.customButton.addEventListener('click', this.handleClick.bind(this))
        // this.createMoreButtons()
        // tool.appendChild(this.customButton)

        // tool.appendChild(this.customDiv)
        document.addEventListener('click', this.hideMoreMenuOnClickOutside.bind(this));
    }

    // createMoreButtons() {
    //     for (let i = 0; i < 3; i++) {
    //         const button = document.createElement('button')
    //         button.classList.add('btn', 'more-menu_btn')
    //         if (i === 0) {
    //             button.textContent = 'Архівувати'
    //             button.classList.add('archive')
    //         } else if (i === 1) {
    //             button.textContent = 'Редагувати'
    //             button.classList.add('edit')
    //         } else if (i === 2) {
    //             button.textContent = 'Видалити'
    //             button.classList.add('delete')
    //             button.addEventListener('click', () => this.deleteCard(this.cardId))
    //         }
    //         this.customDiv.appendChild(button)
    //     }
    // }

    // deleteCard(cardId) {
    //     if (confirm("Дійсно видалити ?")) {
    //         fetch('/control-panel/movies/' + cardId, {
    //             method: 'POST',
    //             headers: {
    //                 'Content-Type': 'application/json'
    //             },
    //             body: JSON.stringify({
    //                 cardId: cardId
    //             })
    //         })
    //             .then(response => {
    //                 if (response.ok) {
    //                     window.location.reload();
    //                 } else {
    //                     throw new Error('Виникла помилка під час видалення.');
    //                 }
    //             })
    //             .catch(error => {
    //                 console.error('Помилка:', error);
    //             });
    //     }
    // }

    // handleClick() {
    //     this.customDiv.style.display = this.customDiv.style.display === 'block'
    //         ? 'none' : 'block'
    //     this.customButton.style.backgroundColor = this.customButton.style.backgroundColor === 'var(--mine-shaft)'
    //         ? '' : 'var(--mine-shaft)'
    // }
    //
    // hideMoreMenuOnClickOutside(event) {
    //     if (!this.customButton.contains(event.target) && !this.customDiv.contains(event.target)) {
    //         this.customDiv.style.display = 'none'
    //         this.customButton.style.backgroundColor = ''
    //     }
    // }
}





































// class MovieSorter {
//     constructor(containerSelector) {
//         this.container = document.querySelector(containerSelector);
//         if (!this.container) return;
//
//         this.popupBtn = this.container.querySelector('.btn.more__btn.sorting-more__btn');
//         this.toolControlsPopup = this.container.querySelector('.mm-popup');
//
//         this.popupBtn.addEventListener('click', () => this.handleClick());
//         document.addEventListener('click', (event) => this.handleDocumentClick(event));
//
//         this.initSortParam();
//         this.setupSortRadio();
//     }
//
//     initSortParam() {
//         const urlParams = new URLSearchParams(window.location.search);
//         this.sortParam = urlParams.get('sort');
//     }
//
//     setupSortRadio() {
//         if (this.sortParam) {
//             const radioBtn = this.container.querySelector(`input[name="sort"][value="${this.sortParam}"]`);
//             if (radioBtn) {
//                 radioBtn.checked = true;
//             }
//         }
//
//         this.container.querySelectorAll('input[name="sort"]').forEach(radioBtn => {
//             radioBtn.addEventListener('change', () => {
//                 this.sortMovies(radioBtn.value);
//             });
//         });
//     }
//
//     sortMovies(sortBy) {
//         window.location.href = "/control-panel/movies?sort=" + sortBy;
//     }
//
//     handleClick() {
//         this.toolControlsPopup.classList.toggle('show');
//         this.popupBtn.classList.toggle('active');
//     }
//
//     handleDocumentClick(event) {
//         if (!this.container.contains(event.target)) {
//             this.toolControlsPopup.classList.remove('show');
//             this.popupBtn.classList.remove('active');
//         }
//     }
// }
//
// document.addEventListener('DOMContentLoaded', () => {
//     new MovieSorter('.mm-popup-cont.movie-sort-tool');
// });























































































// document.addEventListener('DOMContentLoaded', () => {
//     const movieSearchComponents = document.querySelectorAll('.movies-search__comp');
//     movieSearchComponents.forEach(component => new MoviesSearch(component));
//
//     const sortToolContainer = document.querySelector('.mm-popup-cont.movie-sort-tool');
//     if (sortToolContainer) {
//         new MovieSorter(sortToolContainer);
//     }
//
//     const movieTools = document.querySelectorAll('.mm-popup-cont.movie-tools');
//     movieTools.forEach(tool => new MoreMenu(tool));
// });
//
// class MoreMenu {
//     constructor(tool) {
//         if (!tool) return;
//
//         this.tool = tool;
//         this.cardId = tool.dataset.cardId;
//         this.kmBtn = tool.querySelector('.btn.more__btn');
//         this.kmBtnDelete = tool.querySelector('.btn.popup__btn.delete');
//         this.toolControlsPopup = tool.querySelector('.mm-popup');
//
//         this.kmBtn.addEventListener('click', () => this.handleClick());
//         this.kmBtnDelete.addEventListener('click', event => this.deleteCard(event));
//         document.addEventListener('click', event => this.hideMoreMenuOnClickOutside(event));
//     }
//
//     handleClick() {
//         this.toolControlsPopup.style.display = this.toolControlsPopup.style.display === 'block' ? 'none' : 'block';
//         this.kmBtn.style.backgroundColor = this.kmBtn.style.backgroundColor === 'var(--mine-shaft)' ? '' : 'var(--mine-shaft)';
//     }
//
//     hideMoreMenuOnClickOutside(event) {
//         if (!this.tool.contains(event.target)) {
//             this.toolControlsPopup.style.display = 'none';
//             this.kmBtn.style.backgroundColor = '';
//         }
//     }
//
//     async deleteCard(event) {
//         event.stopPropagation();
//         if (confirm("Дійсно видалити?")) {
//             try {
//                 const response = await fetch(`/control-panel/movies/${this.cardId}`, {
//                     method: 'POST',
//                     headers: {
//                         'Content-Type': 'application/json'
//                     },
//                     body: JSON.stringify({ cardId: this.cardId })
//                 });
//                 if (!response.ok) throw new Error('Виникла помилка під час видалення.');
//                 window.location.reload();
//             } catch (error) {
//                 console.error('Помилка:', error);
//             }
//         }
//     }
// }
//
// class MoviesSearch {
//     constructor(tool) {
//         if (!tool) return;
//
//         this.customInput = tool.querySelector('.search__inp');
//         this.customButton = tool.querySelector('.search__btn');
//         this.movieListContainer = document.querySelector('.movie-list_container');
//
//         this.customInput.addEventListener('input', this.searchMovies.bind(this));
//         this.customButton.addEventListener('click', this.searchMovies.bind(this));
//     }
//
//     async searchMovies() {
//         const searchString = this.customInput.value;
//         try {
//             const response = await fetch(`/control-panel/movies?search=${encodeURIComponent(searchString)}`);
//             if (!response.ok) throw new Error('Network response was not ok');
//             const html = await response.text();
//             const parser = new DOMParser();
//             const doc = parser.parseFromString(html, "text/html");
//             const movieListFragment = doc.querySelector('.movie-list_container');
//             this.movieListContainer.innerHTML = '';
//             movieListFragment.childNodes.forEach(child => {
//                 this.movieListContainer.appendChild(child.cloneNode(true));
//             });
//         } catch (error) {
//             console.error('Error:', error);
//         }
//     }
// }
//
// class MovieSorter {
//     constructor(container) {
//         this.container = container;
//         if (!this.container) return;
//
//         this.popupBtn = this.container.querySelector('.btn.more__btn.sorting-more__btn');
//         this.toolControlsPopup = this.container.querySelector('.mm-popup');
//
//         this.popupBtn.addEventListener('click', this.handleClick.bind(this));
//         document.addEventListener('click', this.handleDocumentClick.bind(this));
//
//         this.initSortParam();
//         this.setupSortRadio();
//     }
//
//     initSortParam() {
//         const urlParams = new URLSearchParams(window.location.search);
//         this.sortParam = urlParams.get('sort');
//     }
//
//     setupSortRadio() {
//         if (this.sortParam) {
//             const selectedRadioButton = this.container.querySelector(`input[name="sort"][value="${this.sortParam}"]`);
//             if (selectedRadioButton) {
//                 selectedRadioButton.checked = true;
//             }
//         }
//
//         this.container.querySelectorAll('input[name="sort"]').forEach(radioBtn => {
//             radioBtn.addEventListener('change', () => {
//                 this.sortMovies(radioBtn.value);
//             });
//         });
//     }
//
//     sortMovies(sortBy) {
//         window.location.href = `/control-panel/movies?sort=${sortBy}`;
//     }
//
//     handleClick() {
//         this.toolControlsPopup.classList.toggle('show');
//         this.popupBtn.classList.toggle('active');
//     }
//
//     handleDocumentClick(event) {
//         if (!this.container.contains(event.target)) {
//             this.toolControlsPopup.classList.remove('show');
//             this.popupBtn.classList.remove('active');
//         }
//     }
// }


















































// class Popup {
//     constructor(triggerSelector, popupSelector) {
//         this.trigger = document.querySelector(triggerSelector);
//         this.popup = document.querySelector(popupSelector);
//         this.isOpen = false;
//
//         this.init();
//     }
//
//     init() {
//         this.trigger.addEventListener('click', () => this.togglePopup());
//         window.addEventListener('click', (e) => this.windowOnClick(e));
//         window.addEventListener('keydown', (e) => this.windowOnKeyDown(e));
//         this.popup.addEventListener('click', (e) => this.stopPropagation(e));
//     }
//
//     togglePopup() {
//         this.isOpen = !this.isOpen;
//         if (this.isOpen) {
//             this.popup.style.display = 'block';
//         } else {
//             this.popup.style.display = 'none';
//         }
//     }
//
//     windowOnClick(event) {
//         if (this.isOpen && event.target !== this.popup && !this.popup.contains(event.target)) {
//             this.togglePopup();
//         }
//     }
//
//     windowOnKeyDown(event) {
//         if (this.isOpen && event.key === 'Escape') {
//             this.togglePopup();
//         }
//     }
//
//     stopPropagation(event) {
//         event.stopPropagation();
//     }
// }
//
// document.querySelectorAll('.popup-container').forEach(container => {
//     const trigger = container.querySelector('.trigger');
//     const popup = container.querySelector('.popup');
//
//     new Popup(`#${trigger.id}`, `#${popup.id}`);
// });

















































