
// horizontal scroll movie cards

let scrollContainer,
    cardWidth,
    lastScrollTime,
    progressBarStatus,
    progressBarContent,
    scrollBtnToLeft,
    scrollBtnToRight

function handleScroll(e) {
    e.preventDefault()
    const now = Date.now()
    const scrollAmount = e.deltaY > 0 ? cardWidth : -cardWidth

    if (now - lastScrollTime < 150) return

    if (scrollContainer.scrollLeft + scrollContainer.offsetWidth >= scrollContainer.scrollWidth && e.deltaY > 0) {
        window.scrollBy({ top: scrollAmount, behavior: 'smooth' })
    } else {
        smoothScroll(scrollContainer, scrollAmount)
    }
    lastScrollTime = now
    progressBarHorizontal()
    checkButtonState()
}

function handleMediaQuery(mediaQuery) {
    scrollContainer = document.getElementById('mid-card__container_horizontal')
    cardWidth = document.querySelector('.tt-card__item-film').offsetWidth
    progressBarStatus = document.getElementById('mid-card__progress-bar-status')
    progressBarContent = document.querySelector('.tt-card__progress-bar-content')
    scrollBtnToLeft = document.querySelector('.tt-card-toleft')
    scrollBtnToRight = document.querySelector('.tt-card-toright')

    lastScrollTime = 0

    if (mediaQuery.matches) {
        window.removeEventListener('scroll', progressBarVertical)
        scrollContainer.addEventListener('wheel', handleScroll, { passive: false })
        scrollBtnToRight.addEventListener('click', () => handleButtonClick(cardWidth * 2))
        scrollBtnToLeft.addEventListener('click', () => handleButtonClick(-cardWidth * 2))
    } else {
        scrollContainer.removeEventListener('wheel', handleScroll)
        window.addEventListener('scroll', progressBarVertical)
        scrollBtnToRight.removeEventListener('click', () => handleButtonClick(cardWidth * 2))
        scrollBtnToLeft.removeEventListener('click', () => handleButtonClick(-cardWidth * 2))
    }
}

function handleButtonClick(scrollAmount) {
    const now = Date.now()
    if (now - lastScrollTime < 150) return
    smoothScroll(scrollContainer, scrollAmount)
    lastScrollTime = now
    progressBarHorizontal()
    checkButtonState()
}

function checkButtonState() {
    if (scrollContainer.scrollLeft === 0) {
        scrollBtnToLeft.style.display = 'none'
    } else {
        scrollBtnToLeft.style.display = 'block'
    }

    if (scrollContainer.scrollLeft + scrollContainer.offsetWidth >= scrollContainer.scrollWidth) {
        scrollBtnToRight.style.display = 'none'
    } else {
        scrollBtnToRight.style.display = 'block'
    }
}

const mediaQuery1024 = window.matchMedia('(min-width: 1024px)')
mediaQuery1024.addListener(handleMediaQuery)
handleMediaQuery(mediaQuery1024)

function smoothScroll(element, scrollAmount) {
    const startPosition = element.scrollLeft
    const targetPosition = startPosition + scrollAmount
    const duration = 0
    const startTime = performance.now()

    function scroll(timestamp) {
        const currentTime = timestamp - startTime
        const progress = Math.min(currentTime / duration, 1)
        element.scrollLeft =
            startPosition + (targetPosition - startPosition) * progress
        if (currentTime < duration) {
            requestAnimationFrame(scroll)
        }
    }
    requestAnimationFrame(scroll)
}

let totalScroll, scrolled
function progressBarHorizontal() {
    totalScroll = scrollContainer.scrollWidth - scrollContainer.clientWidth
    scrolled = (scrollContainer.scrollLeft / totalScroll) * 100
    progressBarStatus.style.width = scrolled + '%'
}
function progressBarVertical() {
    if (header.classList.contains('hide')) {
        progressBarContent.style.top = `${0}px`
    } else {
        progressBarContent.style.top = header.offsetHeight + 2 + 'px'
    }
    totalScroll = document.documentElement.scrollHeight - document.documentElement.clientHeight
    scrolled = (window.scrollY / totalScroll) * 100
    progressBarStatus.style.width = scrolled + '%'
}






























// let scrollContainer,
//     cardWidth,
//     lastScrollTime,
//     progressBarStatus,
//     progressBarContent
//
// function handleScroll(e) {
//     e.preventDefault()
//     const now = Date.now()
//     const scrollAmount = e.deltaY > 0 ? cardWidth : -cardWidth
//
//     if (now - lastScrollTime < 150) return
//
//     if (scrollContainer.scrollLeft + scrollContainer.offsetWidth >= scrollContainer.scrollWidth && e.deltaY > 0) {
//         window.scrollBy({ top: scrollAmount, behavior: 'smooth' })
//     } else {
//         smoothScroll(scrollContainer, scrollAmount)
//     }
//     lastScrollTime = now
//     progressBarHorizontal()
// }
//
// function handleMediaQuery(mediaQuery) {
//     scrollContainer = document.getElementById('mid-card__container_horizontal')
//     cardWidth = document.querySelector('.tt-card__item-film').offsetWidth
//     progressBarStatus = document.getElementById('mid-card__progress-bar-status')
//     progressBarContent = document.querySelector('.tt-card__progress-bar-content')
//     lastScrollTime = 0
//
//     if (mediaQuery.matches) {
//         window.removeEventListener('scroll', progressBarVertical)
//         scrollContainer.addEventListener('wheel', handleScroll, { passive: false })
//     } else {
//         scrollContainer.removeEventListener('wheel', handleScroll)
//         window.addEventListener('scroll', progressBarVertical)
//     }
// }
//
// const mediaQuery1024 = window.matchMedia('(min-width: 1024px)')
// mediaQuery1024.addListener(handleMediaQuery)
// handleMediaQuery(mediaQuery1024)
//
// function smoothScroll(element, scrollAmount) {
//     const startPosition = element.scrollLeft
//     const targetPosition = startPosition + scrollAmount
//     const duration = 0
//     const startTime = performance.now()
//
//     function scroll(timestamp) {
//         const currentTime = timestamp - startTime
//         const progress = Math.min(currentTime / duration, 1)
//         element.scrollLeft =
//             startPosition + (targetPosition - startPosition) * progress
//         if (currentTime < duration) {
//             requestAnimationFrame(scroll)
//         }
//     }
//     requestAnimationFrame(scroll)
// }
//
// let totalScroll, scrolled
// function progressBarHorizontal() {
//     totalScroll = scrollContainer.scrollWidth - scrollContainer.clientWidth
//     scrolled = (scrollContainer.scrollLeft / totalScroll) * 100
//     progressBarStatus.style.width = scrolled + '%'
// }
// function progressBarVertical() {
//     if (header.classList.contains('hide')) {
//         progressBarContent.style.top = `${0}px`
//     } else {
//         progressBarContent.style.top = header.offsetHeight + 2 + 'px'
//     }
//     totalScroll = document.documentElement.scrollHeight - document.documentElement.clientHeight
//     scrolled = (window.scrollY / totalScroll) * 100
//     progressBarStatus.style.width = scrolled + '%'
// }





















const prevBtn = document.getElementById('hid-info-area-prev');
const nextBtn = document.getElementById('hid-info-area-next');
let slideIndex = 1
let timer
showSlides(slideIndex)
function animateClickBtn(btn) {
    btn.style.transition = 'transform 0.1s, filter 0.1s';
    setTimeout(function () {
        btn.style.transform = 'scale(1)';
        btn.style.filter = 'opacity(0.3) drop-shadow(0px 0px 11px black)';
    }, 100);
    btn.style.transform = 'scale(1.2)';
    btn.style.filter = 'opacity(0.5) drop-shadow(0px 0px 11px black)';
    btn.removeEventListener('click', animateClickBtn);
}

function nextSlide() {
    showSlides((slideIndex += 1), false)
    animateClickBtn(nextBtn);
}

function previousSlide() {
    showSlides((slideIndex -= 1), true)
    animateClickBtn(prevBtn);
}

function currentSlide(n) {showSlides((slideIndex = n))}

function showSlides(n,animType) {
    let slides = document.getElementsByClassName('item')
    let dotsContainer = document.querySelector('.dots-container')
    let dots = ''

    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}

    for (let slide of slides) {slide.style.display = 'none'}

    slides[slideIndex - 1].style.display = 'grid'
    slides[slideIndex - 1].style.animation = animType ? 'fadeNext  0.5s forwards' : 'fadePrev  0.5s forwards'
    for (let i = 0; i < slides.length; i++) {
        dots += '<span class="dot" onclick="currentSlide(' + (i + 1) + ')"></span>'
    }

    dotsContainer.innerHTML = dots

    let dotsArray = document.querySelectorAll('.dot')
    dotsArray.forEach((dot, index) => {
        if (index === slideIndex - 1) {
            dot.classList.add('active')
        } else {
            dot.classList.remove('active')
        }
    })
}

function startAutoSlide() {timer = setInterval(nextSlide, 5000)}

function stopAutoSlide() {clearInterval(timer)}

startAutoSlide()

const containerSlide = document.querySelector('.tt-info-slider')
containerSlide.addEventListener('mouseover', stopAutoSlide)
containerSlide.addEventListener('mouseout', startAutoSlide)











//new code
//movie card popup week sessions
const sessionsDate = document.querySelector('.session-date__popup')
const cardItemMask = document.querySelector('.tt-card__item-film')

const computedStyle = window.getComputedStyle(sessionsDate)
function showDaysPopup() {
    if (computedStyle.display === 'none' || computedStyle.display === '') {
        sessionsDate.style.display = 'flex'
        document.addEventListener('click', closePopupOnClickOutside)
    }
}

cardItemMask.addEventListener('mouseleave', function () {
    sessionsDate.style.display = 'none'
})


function closePopupOnClickOutside(event){
    if (!event.target.closest('#select-session-date')) {
        sessionsDate.style.display = 'none'
        console.log('fwefwe')
        document.removeEventListener('click', closePopupOnClickOutside)
    }
}
